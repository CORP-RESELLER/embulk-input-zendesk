## 0.1.8 - 2016-07-11

* [enhancement] For huge data [#13](https://github.com/treasure-data/embulk-input-zendesk/pull/13)
* [enhancement] Improvements for non incremental export [#12](https://github.com/treasure-data/embulk-input-zendesk/pull/12)

## 0.1.7 - 2016-06-04
* [enhancement] Improvements for non incremental export [#12](https://github.com/treasure-data/embulk-input-zendesk/pull/12)

## 0.1.6 - 2016-05-09
* [fixed] Fix non-incremental export to fetch all records [#11](https://github.com/treasure-data/embulk-input-zendesk/pull/11)

## 0.1.5 - 2016-04-14
* [enhancement] Mitigate debug pain when many retry then error [#10](https://github.com/treasure-data/embulk-input-zendesk/pull/10)

## 0.1.4 - 2016-04-08

* [enhancement] Correct preview data [#9](https://github.com/treasure-data/embulk-input-zendesk/pull/9)

## 0.1.3 - 2016-03-15

* [enhancement] Support more targets [#8](https://github.com/treasure-data/embulk-input-zendesk/pull/8)
* [enhancement] Enable json type [#7](https://github.com/treasure-data/embulk-input-zendesk/pull/7)

## 0.1.2 - 2016-01-29

* [maintenance] Add authors @muga and @sakama.
* [enhancement] Add Incremental option [#6](https://github.com/treasure-data/embulk-input-zendesk/pull/6)

## 0.1.1 - 2016-01-26

* [fixed] Fix when null value given.

## 0.1.0 - 2016-01-26

The first release!!
